import { Component, OnInit, Injector } from '@angular/core';
import { ModalComponentBase } from '../../component-base/modal-component-base';

@Component({
  selector: 'app-share-qrcode',
  templateUrl: './share-qrcode.component.html',
})
export class ShareQrcodeComponent extends ModalComponentBase {
  qrcodeUrl: string;
  constructor(injector: Injector) {
    super(injector);
  }
}
