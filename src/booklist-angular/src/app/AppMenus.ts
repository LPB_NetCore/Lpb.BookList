import { MenuItem } from '@yoyo/theme';

// 全局的左侧导航菜单
export class AppMenus {
  static Menus = [
    // 首页
    new MenuItem('HomePage', '', 'anticon anticon-home', '/app/home'),
    new MenuItem(
      'Tenants',
      'Pages.Tenants',
      'anticon anticon-team',
      '/app/tenants',
    ),
    new MenuItem(
      'Roles',
      'Pages.Roles',
      'anticon anticon-safety',
      '/app/roles',
    ),
    new MenuItem('Users', 'Pages.Users', 'anticon anticon-user', '/app/users'),
    new MenuItem('About', '', 'anticon anticon-info-circle-o', '/app/about'),
    new MenuItem('CloudBookList', '', 'anticon anticon-info-circle-o', '', [
      // 租户 // 角色 // 用户 // 关于我们 // 云书单
      // 书籍
      new MenuItem(
        'Books',
        'Pages.Book',
        'anticon anticon-book',
        '/app/cloud-book-list/book',
      ),
      new MenuItem(
        'BookTag',
        'Pages.BookTag',
        'anticon anticon-book',
        '/app/cloud-book-list/book-tag',
      ),
      // 书单列表
      new MenuItem(
        'CloudBookList',
        'Pages.CloudBookList',
        'anticon anticon-book',
        '/app/cloud-book-list/cloud-book-list',
      ),
    ]),
  ];
}
