import {
  Component,
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
  Injector,
  Input,
} from '@angular/core';
import { NzSelectComponent } from 'ng-zorro-antd';
import { AppComponentBase } from '../../../../shared/component-base/app-component-base';
import {
  BookTagServiceProxy,
  CreateOrUpdateBookTagInput,
  BookTagEditDto,
} from '../../../../shared/service-proxies/service-proxies';
import { forEach } from '@angular/router/src/utils/collection';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-booktag-nzselect',
  templateUrl: './booktag-nzselect.component.html',
  styles: [],
})
export class BooktagNzselectComponent extends AppComponentBase
  implements OnInit {
  @ViewChild('booktagselect')
  booktagselect: NzSelectComponent;
  @Output()
  selectedDataChange = new EventEmitter();

  @Input()
  set tagsSourceData(value: any) {
    this.isLoading = true;
    if (value) {
      this.listOfTagOptions = value;
      this.listOfSelectedValue = [];

      this.listOfTagOptions.forEach(item => {
        if (item.isSelected) {
          this.listOfSelectedValue.push(item.id);
        }
      });
    }

    if (this.selectedDataChange) {
      this.selectedDataChange.emit(this.listOfSelectedValue);
    }
    this.isLoading = false;
  }

  isLoading = true;
  listOfTagOptions = [];
  listOfSelectedValue = [];

  searchValue = '';

  constructor(
    injector: Injector,
    private _booktagService: BookTagServiceProxy,
  ) {
    super(injector);
  }

  handleInputConfirm(e): void {
    // 过滤掉已经存在的值

    const booktagselectValues = this.booktagselect.listOfSelectedValue;

    for (let index = 0; index < booktagselectValues.length; index++) {
      const element = booktagselectValues[index];

      if (typeof element === 'number') {
        // console.log('已经存在于服务器的值');
      } else {
        // 检查 当前用户是否有权限创建booktag

        if (this.permission.isGranted('Pages.BookTag.Create')) {
          this.isLoading = true;
          const BooktageditDto: BookTagEditDto = new BookTagEditDto();
          BooktageditDto.tagName = element;
          // tslint:disable-next-line:no-debugger
          // debugger;
          this._booktagService
            .create(BooktageditDto) // 无返回值
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe(res => {
              // console.log(res);
              const listOfSelectedValue = this.listOfSelectedValue;
              const listTagOptions = this.listOfTagOptions;
              for (
                let selectIndex = 0;
                selectIndex < listOfSelectedValue.length;
                selectIndex++
              ) {
                if (res.tagName === listOfSelectedValue[selectIndex]) {
                  listTagOptions.push(res);
                  listOfSelectedValue[selectIndex] = res.id;
                }
              }
            });
        }
      }
    }
  }

  modelchange(): void {
    if (this.selectedDataChange) {
      this.selectedDataChange.emit(this.listOfSelectedValue);
    }
  }

  ngOnInit() {}
}
