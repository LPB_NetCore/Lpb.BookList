import { BooktagNzselectComponent } from './components/booktag-nzselect/booktag-nzselect.component';
import { AbpModule, LocalizationService } from '@yoyo/abp';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CloudBookListRoutingModule } from './cloud-book-list-routing.module';
import { SharedModule } from '@shared/shared.module';
import { TitleService } from 'yoyo-ng-module/src/theme';
import { BookComponent } from './books/book.component';
import { CreateOrEditBookComponent } from './books/create-or-edit-book/create-or-edit-book.component';
import { BookTagComponent } from './book-tags/book-tag.component';
import { CreateOrEditBookTagComponent } from './book-tags/create-or-edit-book-tag/create-or-edit-book-tag.component';
import { ImgShowComponent } from './components/img-show/img-show.component';
import { CloudBookListComponent } from './cloud-book-lists/cloud-book-list.component';
// tslint:disable-next-line:max-line-length
import { CreateOrEditCloudBookListComponent } from './cloud-book-lists/create-or-edit-cloud-book-list/create-or-edit-cloud-book-list.component';
import { BookNzselectComponent } from './components/book-nzselect/book-nzselect.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule,
    AbpModule,
    CloudBookListRoutingModule,
  ],
  declarations: [
    BookComponent,
    CreateOrEditBookComponent,
    BookTagComponent,
    CreateOrEditBookTagComponent,
    ImgShowComponent,
    BooktagNzselectComponent,
    CloudBookListComponent,
    CreateOrEditCloudBookListComponent,
    BookNzselectComponent,
  ],
  entryComponents: [
    BookComponent,
    CreateOrEditBookComponent,
    BookTagComponent,
    CreateOrEditBookTagComponent,
    ImgShowComponent,
    BooktagNzselectComponent,
    CloudBookListComponent,
    CreateOrEditCloudBookListComponent,
    BookNzselectComponent,
  ],
  providers: [LocalizationService, TitleService],
})
export class CloudBookListModule {}
