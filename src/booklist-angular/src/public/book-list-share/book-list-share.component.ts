import { finalize } from 'rxjs/operators';
import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/component-base';
import { BookServiceProxy } from '@shared/service-proxies/service-proxies';
import { ActivatedRoute } from '@angular/router';
import { CloudBookListShareDto } from '@shared/service-proxies/service-proxies';
import { AppConsts } from '@shared/AppConsts';
import { ShareQrcodeComponent } from '@shared/components/share-qrcode/share-qrcode.component';

@Component({
  selector: 'app-book-list-share',
  templateUrl: './book-list-share.component.html',
  styles: [],
})
export class BookListShareComponent extends AppComponentBase implements OnInit {
  id: number;
  tid: number;
  loading = false;
  entity: CloudBookListShareDto;

  constructor(
    injector: Injector,
    private _activateRoute: ActivatedRoute,
    private _bookService: BookServiceProxy,
  ) {
    super(injector);
  }

  ngOnInit() {
    this.id = this._activateRoute.snapshot.params['id'];
    this.tid = this._activateRoute.snapshot.params['tid'];

    // 查询数据

    if (this.tid && this.id) {
      this._bookService
        .getBookListShare(this.id, this.tid)
        .pipe(
          finalize(() => {
            this.loading = false;
          }),
        )
        .subscribe(result => {
          this.entity = result;
        });
    }
  }

  // 分享二维码功能

  shareQrCode() {
    const tid = this.tid;
    const cloudbookListId = this.id;
    const url =
      AppConsts.appBaseUrl +
      '/public/book-list-share;tid=' +
      tid +
      ';id=' +
      cloudbookListId;
    this.modalHelper
      .open(ShareQrcodeComponent, { qrcodeUrl: url }, 'sm')
      .subscribe(() => {});
  }
}
