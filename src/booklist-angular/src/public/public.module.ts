import { AbpModule } from '@yoyo/abp';
import { HttpModule, JsonpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicComponent } from './public.component';
import { FormsModule } from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { SharedModule } from '@shared/shared.module';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { PublicRoutingModule } from './public-routing.module';
import { BookListShareComponent } from './book-list-share/book-list-share.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    NgZorroAntdModule,
    AbpModule,
    SharedModule,
    ServiceProxyModule,
    PublicRoutingModule,
  ],
  declarations: [BookListShareComponent, PublicComponent],
})
export class PublicModule {}
