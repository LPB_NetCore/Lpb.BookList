import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styles: ['./public.component.less'],
})
export class PublicComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
