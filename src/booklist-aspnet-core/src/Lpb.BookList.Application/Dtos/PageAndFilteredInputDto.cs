﻿using Abp;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;

namespace Lpb.BookList.Dtos
{
    public class PageAndFilteredInputDto : PagedAndSortedResultRequestDto
    {
        /// <summary>
        /// 过滤文本
        /// </summary>
        public string FilterText { get; set; }      
        
        
    }
}