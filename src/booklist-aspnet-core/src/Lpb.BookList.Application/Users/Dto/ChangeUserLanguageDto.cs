using System.ComponentModel.DataAnnotations;

namespace Lpb.BookList.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}