using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Lpb.BookList.MultiTenancy.Dto;

namespace Lpb.BookList.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
