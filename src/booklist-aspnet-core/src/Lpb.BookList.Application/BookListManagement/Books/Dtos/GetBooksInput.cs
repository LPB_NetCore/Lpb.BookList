
using Abp.Runtime.Validation;
using Lpb.BookList.Dtos;
using Lpb.BookList.BookListManagement.Books;

namespace Lpb.BookList.BookListManagement.Books.Dtos
{
    public class GetBooksInput : PagedSortedAndFilteredInputDto, IShouldNormalize
    {

        /// <summary>
        /// 正常化排序使用
        /// </summary>
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }

    }
}
