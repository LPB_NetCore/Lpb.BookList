

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Lpb.BookList.BookListManagement.Books;

namespace Lpb.BookList.BookListManagement.Books.Dtos
{
    public class CreateOrUpdateBookInput
    {
        [Required]
        public BookEditDto Book { get; set; }


        public List<long> TagIds { get; set; }

    }
}