﻿using System.Collections.Generic;

namespace Lpb.BookList.BookListManagement.Books.Dtos
{
    public class BookIncludeTagDto
    {
        /// <summary>
        /// 书名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 简介
        /// </summary>
        public string Intro { get; set; }

        /// <summary>
        /// 购买链接
        /// </summary>
        public string PriceUrl { get; set; }

        /// <summary>
        /// 图片链接
        /// </summary>
        public string ImgUrl { get; set; }

        public List<string> BookTags { get; set; }
    }
}