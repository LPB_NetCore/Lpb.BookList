

using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Lpb.BookList.BookListManagement.Books;
using Lpb.BookList.BookListManagement.BookTags.Dtos;

namespace Lpb.BookList.BookListManagement.Books.Dtos
{
    public class GetBookForEditOutput
    {

        public BookEditDto Book { get; set; }

        public List<BookTagSelectListDto> BookTags { get; set; }

    }
}