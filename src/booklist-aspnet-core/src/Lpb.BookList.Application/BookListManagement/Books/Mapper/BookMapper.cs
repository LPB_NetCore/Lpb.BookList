
using AutoMapper;
using Lpb.BookList.BookListManagement.Books;
using Lpb.BookList.BookListManagement.Books.Dtos;

namespace Lpb.BookList.BookListManagement.Books.Mapper
{

    /// <summary>
    /// 配置Book的AutoMapper
    /// </summary>
    internal static class BookMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Book, BookListDto>();
            configuration.CreateMap<BookListDto, Book>();

            configuration.CreateMap<BookEditDto, Book>();
            configuration.CreateMap<Book, BookEditDto>();

            configuration.CreateMap<Book, BookSelectListDto>()
                .ForMember(a => a.IsSelected, options => options.Ignore());

            configuration.CreateMap<Book, BookIncludeTagDto>()
                .ForMember(a => a.BookTags, memberOptions: opt => opt.Ignore());
        }
    }
}
