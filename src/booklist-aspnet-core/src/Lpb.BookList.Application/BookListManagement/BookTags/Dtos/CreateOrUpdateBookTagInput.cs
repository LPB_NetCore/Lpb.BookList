

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Lpb.BookList.BookListManagement.BookTags;

namespace Lpb.BookList.BookListManagement.BookTags.Dtos
{
    public class CreateOrUpdateBookTagInput
    {
        [Required]
        public BookTagEditDto BookTag { get; set; }

    }
}