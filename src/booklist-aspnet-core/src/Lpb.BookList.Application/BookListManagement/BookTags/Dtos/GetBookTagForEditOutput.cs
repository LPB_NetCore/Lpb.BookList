

using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Lpb.BookList.BookListManagement.BookTags;

namespace Lpb.BookList.BookListManagement.BookTags.Dtos
{
    public class GetBookTagForEditOutput
    {

        public BookTagEditDto BookTag { get; set; }

    }
}