
using Abp.Runtime.Validation;
using Lpb.BookList.Dtos;
using Lpb.BookList.BookListManagement.BookTags;

namespace Lpb.BookList.BookListManagement.BookTags.Dtos
{
    public class GetBookTagsInput : PagedSortedAndFilteredInputDto, IShouldNormalize
    {

        /// <summary>
        /// 正常化排序使用
        /// </summary>
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }

    }
}
