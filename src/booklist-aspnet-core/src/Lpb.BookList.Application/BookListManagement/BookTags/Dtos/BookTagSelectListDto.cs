﻿namespace Lpb.BookList.BookListManagement.BookTags.Dtos
{
    public class BookTagSelectListDto : BookTagListDto
    {
        /// <summary>
        /// 是否选中
        /// </summary>
        public bool IsSelected { get; set; }

    }
}