
using System;
using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities.Auditing;
using Lpb.BookList.BookListManagement.BookTags;

namespace  Lpb.BookList.BookListManagement.BookTags.Dtos
{
    public class BookTagEditDto
    {

        /// <summary>
        /// Id
        /// </summary>
        public long? Id { get; set; }         


        
		/// <summary>
		/// 标签名称
		/// </summary>
		[MaxLength(55, ErrorMessage="标签名称超出最大长度")]
		[Required(ErrorMessage="标签名称不能为空")]
		public string TagName { get; set; }




    }
}