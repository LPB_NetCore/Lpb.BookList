
using AutoMapper;
using Lpb.BookList.BookListManagement.CloudBookLists;
using Lpb.BookList.BookListManagement.CloudBookLists.Dtos;

namespace Lpb.BookList.BookListManagement.CloudBookLists.Mapper
{

	/// <summary>
    /// 配置CloudBookList的AutoMapper
    /// </summary>
	internal static class CloudBookListMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap <CloudBookList,CloudBookListListDto>();
            configuration.CreateMap <CloudBookListListDto,CloudBookList>();

            configuration.CreateMap <CloudBookListEditDto,CloudBookList>();
            configuration.CreateMap <CloudBookList,CloudBookListEditDto>();

            configuration.CreateMap<CloudBookList, CloudBookListShareDto>()
                .ForMember(o => o.UserName, opt => opt.Ignore())
                .ForMember(o => o.Books, opt => opt.Ignore());

        }
	}
}
