﻿using System;
using System.Collections.Generic;
using Lpb.BookList.BookListManagement.Books.Dtos;

namespace Lpb.BookList.BookListManagement.CloudBookLists.Dtos
{
    public class CloudBookListShareDto
    {
        public string Name { get; set; }


        public string Intro { get; set; }

        public DateTime CreationTime { get; set; }

        public List<BookIncludeTagDto> Books { get; set; }

        public string UserName { get; set; }
    }
}