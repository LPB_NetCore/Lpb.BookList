
using System;
using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities.Auditing;
using Lpb.BookList.BookListManagement.CloudBookLists;

namespace  Lpb.BookList.BookListManagement.CloudBookLists.Dtos
{
    public class CloudBookListEditDto
    {

        /// <summary>
        /// Id
        /// </summary>
        public long? Id { get; set; }         


        
		/// <summary>
		/// 名称
		/// </summary>
		public string Name { get; set; }



		/// <summary>
		/// 简介
		/// </summary>
		public string Intro { get; set; }




    }
}