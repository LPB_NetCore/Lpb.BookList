
using Abp.Runtime.Validation;
using Lpb.BookList.Dtos;
using Lpb.BookList.BookListManagement.CloudBookLists;

namespace Lpb.BookList.BookListManagement.CloudBookLists.Dtos
{
    public class GetCloudBookListsInput : PagedSortedAndFilteredInputDto, IShouldNormalize
    {

        /// <summary>
        /// 正常化排序使用
        /// </summary>
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }

    }
}
