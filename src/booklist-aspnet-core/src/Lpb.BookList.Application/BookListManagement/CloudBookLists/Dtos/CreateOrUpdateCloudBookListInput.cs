

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Lpb.BookList.BookListManagement.CloudBookLists;

namespace Lpb.BookList.BookListManagement.CloudBookLists.Dtos
{
    public class CreateOrUpdateCloudBookListInput
    {
        [Required]
        public CloudBookListEditDto CloudBookList { get; set; }

        public List<long> BookIds { get; set; }

    }
}