

using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Lpb.BookList.BookListManagement.Books.Dtos;
using Lpb.BookList.BookListManagement.CloudBookLists;

namespace Lpb.BookList.BookListManagement.CloudBookLists.Dtos
{
    public class GetCloudBookListForEditOutput
    {

        public CloudBookListEditDto CloudBookList { get; set; }

        public List<BookSelectListDto> Books { get; set; }


    }
}