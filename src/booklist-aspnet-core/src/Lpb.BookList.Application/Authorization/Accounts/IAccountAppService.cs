using System.Threading.Tasks;
using Abp.Application.Services;
using Lpb.BookList.Authorization.Accounts.Dto;

namespace Lpb.BookList.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
