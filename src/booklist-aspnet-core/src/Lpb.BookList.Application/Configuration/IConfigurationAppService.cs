using System.Threading.Tasks;
using Lpb.BookList.Configuration.Dto;

namespace Lpb.BookList.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
