using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Lpb.BookList.Authorization;
using Lpb.BookList.BookListManagement.Books.Authorization;
using Lpb.BookList.BookListManagement.Books.Mapper;
using Lpb.BookList.BookListManagement.BookTags.Authorization;
using Lpb.BookList.BookListManagement.BookTags.Mapper;
using Lpb.BookList.BookListManagement.CloudBookLists.Authorization;
using Lpb.BookList.BookListManagement.CloudBookLists.Mapper;

namespace Lpb.BookList
{
    [DependsOn(
        typeof(BookListCoreModule),
        typeof(AbpAutoMapperModule))]
    public class BookListApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<BookListAuthorizationProvider>();
            
            // 给Book定义授权信息内容
            Configuration.Authorization.Providers.Add<BookAuthorizationProvider>();
            Configuration.Authorization.Providers.Add<BookTagAuthorizationProvider>();
            Configuration.Authorization.Providers.Add<CloudBookListAuthorizationProvider>();

            // 自定义类型映射
            Configuration.Modules.AbpAutoMapper().Configurators.Add(configuration =>
            {
                // XXXMapper.CreateMappers(configuration);
                BookMapper.CreateMappings(configuration);
                BookTagMapper.CreateMappings(configuration);
                CloudBookListMapper.CreateMappings(configuration);
            });
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(BookListApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
