using System.Threading.Tasks;
using Abp.Application.Services;
using Lpb.BookList.Sessions.Dto;

namespace Lpb.BookList.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
