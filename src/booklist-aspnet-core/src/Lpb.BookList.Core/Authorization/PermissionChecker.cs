using Abp.Authorization;
using Lpb.BookList.Authorization.Roles;
using Lpb.BookList.Authorization.Users;

namespace Lpb.BookList.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
