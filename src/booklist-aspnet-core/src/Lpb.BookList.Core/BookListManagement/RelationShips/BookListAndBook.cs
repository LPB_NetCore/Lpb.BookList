﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Lpb.BookList.BookListManagement.Books;
using Lpb.BookList.BookListManagement.CloudBookLists;

namespace Lpb.BookList.BookListManagement.RelationShips
{
    public class BookListAndBook : CreationAuditedEntity<long>, IMustHaveTenant
    {
        public long CloudBookListId { get; set; }
        public virtual CloudBookList CloudBookList { get; set; }

        public long BookId { get; set; }
        public virtual Book Book { get; set; }
        public int TenantId { get; set; }
    }
}