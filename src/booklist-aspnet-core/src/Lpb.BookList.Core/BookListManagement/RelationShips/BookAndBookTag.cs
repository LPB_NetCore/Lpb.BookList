﻿using Abp.Domain.Entities;
using Lpb.BookList.BookListManagement.Books;
using Lpb.BookList.BookListManagement.BookTags;

namespace Lpb.BookList.BookListManagement.RelationShips
{
    public class BookAndBookTag : Entity<long>, IMustHaveTenant
    {
        public long BookId { get; set; }
        public virtual Book Book { get; set; }

        public long BookTagId { get; set; }
        public virtual BookTag BookTag { get; set; }
        public int TenantId { get; set; }
    }
}