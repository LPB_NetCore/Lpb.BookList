﻿using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Lpb.BookList.BookListManagement.RelationShips;

namespace Lpb.BookList.BookListManagement.Books
{
    /// <summary>
    /// 书籍
    /// </summary>
    public class Book : CreationAuditedEntity<long>, IMustHaveTenant
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        public string Intro { get; set; }
        /// <summary>
        /// 价格链接
        /// </summary>
        public string PriceUrl { get; set; }
        /// <summary>
        /// 封面链接
        /// </summary>
        public string ImgUrl { get; set; }


        public virtual ICollection<BookAndBookTag> BookAndBookTags { get; set; }
        public virtual ICollection<BookListAndBook> BookListAndBooks { get; set; }
        public int TenantId { get; set; }
    }
}
