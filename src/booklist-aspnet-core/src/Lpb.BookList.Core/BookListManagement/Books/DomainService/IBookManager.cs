

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp;
using Abp.Domain.Services;
using Lpb.BookList.BookListManagement.Books;
using Lpb.BookList.BookListManagement.RelationShips;


namespace Lpb.BookList.BookListManagement.Books.DomainService
{
    public interface IBookManager : IDomainService
    {

        /// <summary>
        /// 初始化方法
        ///</summary>
        void InitBook();


        // 约定大于配置
        //不能出现viewmodel  不能出现 dto  原则上只操作model entity

        //todo:处理 书籍标签中间表的逻辑

        /// <summary>
        /// 通过书籍查询标签
        /// </summary>
        /// <param name="bookId"></param>
        /// <returns></returns>
        Task<List<BookAndBookTag>> GetTagsByBookId(long bookId);
        /// <summary>
        /// 通过标签 查询书籍
        /// </summary>
        /// <param name="tagId"></param>
        /// <returns></returns>
        Task<List<BookAndBookTag>> GetBooksByTagId(long tagId);
        /// <summary>
        /// 创建书籍和标签的关联关系
        /// </summary>
        /// <param name="bookId"></param>
        /// <param name="tagIds"></param>
        /// <returns></returns>
        Task CreateBookAndBookTagRelationship(long bookId, List<long> tagIds);





    }
}
