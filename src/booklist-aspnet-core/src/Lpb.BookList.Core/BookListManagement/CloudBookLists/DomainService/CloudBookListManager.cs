

using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Abp.Linq;
using Abp.Linq.Extensions;
using Abp.Extensions;
using Abp.UI;
using Abp.Domain.Repositories;
using Abp.Domain.Services;

using Lpb.BookList;
using Lpb.BookList.BookListManagement.CloudBookLists;
using Lpb.BookList.BookListManagement.RelationShips;


namespace Lpb.BookList.BookListManagement.CloudBookLists.DomainService
{
    /// <summary>
    /// CloudBookList领域层的业务管理
    ///</summary>
    public class CloudBookListManager : BookListDomainServiceBase, ICloudBookListManager
    {

        private readonly IRepository<CloudBookList, long> _repository;
        private readonly IRepository<BookListAndBook, long> _bookListAndBookRepository;
        /// <summary>
        /// CloudBookList的构造方法
        ///</summary>
        public CloudBookListManager(
            IRepository<CloudBookList, long> repository, IRepository<BookListAndBook, long> bookListAndBookRepository)
        {
            _repository = repository;
            _bookListAndBookRepository = bookListAndBookRepository;
        }


        /// <summary>
        /// 初始化
        ///</summary>
        public void InitCloudBookList()
        {
            throw new NotImplementedException();
        }

        public async Task CreateBookListAndBookRelationship(long bookListId, List<long> bookIds)
        {
            await _bookListAndBookRepository.DeleteAsync(p => p.CloudBookListId == bookListId);
            await CurrentUnitOfWork.SaveChangesAsync();

            //添加关系
            var newBooks = new List<long>();
            foreach (var bookId in bookIds)
            {
                if (newBooks.Exists(a => a == bookId))
                {
                    continue;
                }
                await _bookListAndBookRepository.InsertAsync(new BookListAndBook()
                {
                    BookId = bookId,
                    CloudBookListId = bookListId
                });
                newBooks.Add(bookId);
            }
        }

        public async Task DeleteByBookId(long? bookId)
        {
            await _bookListAndBookRepository.DeleteAsync(a => a.BookId == bookId.Value);

        }

        public async Task DeleteByBookId(List<long> bookIds)
        {
            await _bookListAndBookRepository.DeleteAsync(a => bookIds.Contains(a.BookId));
        }

        public async Task DeleteByBookListId(long? bookListId)
        {
            await _bookListAndBookRepository.DeleteAsync(a => a.CloudBookListId == bookListId);
        }

        public async Task DeleteByBookListId(List<long> bookListIds)
        {
            await _bookListAndBookRepository.DeleteAsync(a => bookListIds.Contains(a.CloudBookListId));
        }

        public async Task<List<BookListAndBook>> GetByBookListIdAsync(long? bookListId)
        {
            return await _bookListAndBookRepository.GetAll()
                .AsNoTracking()
                .Where(o => o.CloudBookListId == bookListId.Value)
                .ToListAsync();
        }

        public async Task<List<BookListAndBook>> GetByBookIdAsync(long? bookId)
        {
            return await _bookListAndBookRepository.GetAll()
                .AsNoTracking()
                .Where(o => o.BookId == bookId.Value)
                .ToListAsync();
        }


    }
}
