﻿using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Lpb.BookList.BookListManagement.RelationShips;

namespace Lpb.BookList.BookListManagement.BookTags
{
    /// <summary>
    /// 书籍标签
    /// </summary>
    public class BookTag : CreationAuditedEntity<long>, IMustHaveTenant
    {
        /// <summary>
        /// 标签名称
        /// </summary>
        public string TagName { get; set; }

        public virtual ICollection<BookAndBookTag> BookAndBookTags { get; set; }
        public int TenantId { get; set; }
    }
}
