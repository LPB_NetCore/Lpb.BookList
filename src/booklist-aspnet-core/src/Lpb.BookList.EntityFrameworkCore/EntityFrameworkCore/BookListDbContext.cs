using Abp.Zero.EntityFrameworkCore;
using Lpb.BookList.Authorization.Roles;
using Lpb.BookList.Authorization.Users;
using Lpb.BookList.BookListManagement.Books;
using Lpb.BookList.BookListManagement.BookTags;
using Lpb.BookList.BookListManagement.CloudBookLists;
using Lpb.BookList.BookListManagement.RelationShips;
using Lpb.BookList.MultiTenancy;
using Microsoft.EntityFrameworkCore;

namespace Lpb.BookList.EntityFrameworkCore
{
    public class BookListDbContext : AbpZeroDbContext<Tenant, Role, User, BookListDbContext>
    {
        /* Define a DbSet for each entity of the application */

        public DbSet<Book> Books { get; set; }
        public DbSet<CloudBookList> CloudBookLists { get; set; }
        public DbSet<BookTag> BookTags { get; set; }

        public DbSet<BookAndBookTag> BookAndBookTags { get; set; }
        public DbSet<BookListAndBook> BookListAndBooks { get; set; }

        public BookListDbContext(DbContextOptions<BookListDbContext> options)
            : base(options)
        {
        }
    }
}
